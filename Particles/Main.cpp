#define GLEW_STATIC

#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>
#include <string>
#include <thread>

#include "ShaderFactory.h"

//GLM
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtx\transform.hpp"

using namespace glm;


int main()
{
	const int maxParticles = 3000;
	GLfloat particlesContainter[maxParticles];

	 srand (time(0));
	// SFML creates window
	sf::RenderWindow window(sf::VideoMode(800, 600), "Make it Rain",sf::Style::Close);
	sf::Clock clock = sf::Clock();

	// init GLEW
	glewExperimental = GL_TRUE;
	glewInit();

	for (int i = 0; i < maxParticles - 3; i += 3)
	{
		particlesContainter[i] = ((rand()%1000)/50.0f) - 1;
		particlesContainter[i + 1] = ((rand()%1000)/50.0f) - 1;
		particlesContainter[i + 2] = rand()%100;
	}

	// generate the buffer, bind it making it the active buffer and then input the data
	GLuint particles_buffer;
	glGenBuffers(1, &particles_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, particles_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(particlesContainter), particlesContainter, GL_DYNAMIC_DRAW);

	//GLint posAttrib = glGetAttribLocation(ShaderFactory::GetInstance()->GetShaderAtIndex(0), "posID");
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	ShaderFactory::GetInstance()->LoadShaderFromFile("shaders/shader.vert", "shaders/shader.frag");
	glUseProgram(ShaderFactory::GetInstance()->GetShaderAtIndex(0));
	GLuint elapsedTimeUniform = glGetUniformLocation(ShaderFactory::GetInstance()->GetShaderAtIndex(0), "time");
	GLuint idUni = glGetUniformLocation(ShaderFactory::GetInstance()->GetShaderAtIndex(0), "id");

	while(window.isOpen())
	{
		float current = clock.getElapsedTime().asSeconds();

		glUniform1f(elapsedTimeUniform, current);
        //std::cout<<"Time: " << current << std::endl;
		#pragma region //closes the window
		sf::Event windowEvent;

		while (window.pollEvent(windowEvent))
		{
			switch(windowEvent.type)
			{
				//Stop the program from running after the window is closed
				case sf::Event::Closed:
					window.close();
					break;

				// Allows the escape key to close window
				case sf::Event::KeyPressed:
					if(windowEvent.key.code == sf::Keyboard::Escape)
						window.close();
					break;
			}
		}
#pragma endregion
		// clear the screen to black
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

		glBindBuffer(GL_ARRAY_BUFFER, particles_buffer);
		glEnableVertexAttribArray(0);

		glDrawArrays(GL_POINTS, 0, maxParticles);
		window.display();
	}

	// clean up after myself.
	glDeleteProgram(ShaderFactory::GetInstance()->shaderProgram);
	glDeleteBuffers(1, &particles_buffer);
}